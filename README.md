# Continuous Integration tool for project PHP #

Step-by-Step Install Jenkins on Ubuntu 16.04

### What is this repository for? ###

* Step 1 — Installing Jenkins
* Step 2 — Starting Jenkins
* Step 3 — Opening the Firewall
* Step 4 — Setting up Jenkins

### 1. Step 1 — Installing Jenkins ###
* First, add the repository key.
  ```sh   
	$ wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
  ```   	
* Next, we'll append the Debian package repository address to the server's sources.list:
  ```sh
	$ sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
  ```
* Update so that apt-get will use the new repository
  ```sh   	
	$ sudo apt-get update
  ```   	
* Finally, we'll install Jenkins and its dependencies, including Java:
  ```   	
	$ udo apt-get install jenkins
  ```

### 2. Step 2 — Starting Jenkins ###
* Using systemctl we'll start Jenkins:
  ```sh   	
	$ sudo systemctl start jenkins
  ```
* to verify that it started successfully:
  ```sh   	
	$ sudo systemctl status jenkins
  ```

### 3. Step 3 — Opening the Firewall ###

* By default, Jenkins runs on port `8080`, so we'll open that port using ufw ( Uncomplicated Firewall, is an interface to iptables): 
  ```sh	
	$ sudo ufw allow 8080
  ```
* If we enabled our UFW firewall now, it would deny all incoming connections.
  ```sh	
	$ sudo ufw allow 22
  ```
* To enable UFW, use this command:
  ```sh   	
	$ sudo ufw enable
  ```
* We can see the new rules by checking UFW's status:
  ```sh   	
	$ sudo ufw status
  ```
 ### 4. Step 4 — Setting up Jenkins ###
 * http://ip_address_or_domain_name:8080